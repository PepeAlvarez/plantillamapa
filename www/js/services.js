angular.module("starter.services", [])
.factory("leeArchivoJSON", function($http, $q) {
  return {
    getData: function(archivo) {
      var deferred = $q.defer();
      $http.get(archivo).success(function(data) {
        deferred.resolve(data);
      }).error(function() {
        deferred.reject();
      });
      //console.log(deferred.promise);
      return deferred.promise;
    }
  }
})

.factory("manejaMarcadores", function(leeArchivoJSON){
  var creaMarcadores =  function(map, archivo){
    leeArchivoJSON.getData(archivo).then(
      function(data) {
        var datos = data;
        var marcadores = [];
        for (i = 0; i < datos.length; i++) {
          marcador = new google.maps.Marker({
            position: new google.maps.LatLng(datos[i].Latitud, datos[i].Longitud),
            map: map});
          marcadores.push(marcador);
        }
      }
    );
  };

  //google.maps.TravelMode.DRIVING (Default) indicates standard driving directions using the road network.
  //google.maps.TravelMode.BICYCLING requests bicycling directions via bicycle paths & preferred streets.
  //google.maps.TravelMode.TRANSIT requests directions via public transit routes.
  //google.maps.TravelMode.WALKING
  var creaRuta = function(map, archivo, modo_viaje){
    leeArchivoJSON.getData(archivo).then(
    function(data) {
      var datos = data;
      var waypts = [];
      directionsService = new google.maps.DirectionsService();
      directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true
      });
      if (datos.length > 1){
        for (var i = 0; i < datos.length; i++) {
          waypts.push({
            location:new google.maps.LatLng(datos[i].Latitud, datos[i].Longitud),
            stopover:true
          });
        };
        var request = {
          origin: new google.maps.LatLng(datos[0].Latitud, datos[0].Longitud),
          destination: new google.maps.LatLng(datos[datos.length - 1].Latitud, datos[datos.length - 1].Longitud),
          waypoints: waypts,
          optimizeWaypoints: true,
          //travelMode: google.maps.DirectionsTravelMode.DRIVING
          travelMode: modo_viaje

        };
        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            polylineOptions = {
              strokeColor: '#808080',
              strokeWeight: '1'
            }
            directionsDisplay.setOptions({
              polylineOptions: polylineOptions
            });
            directionsDisplay.setDirections(response);
          }
        });
        directionsDisplay.setMap(map);
      }
    });
  };
  return {
    crea: creaMarcadores,
    ruta: creaRuta
  };
});
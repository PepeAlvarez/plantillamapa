angular.module('starter.directives', ['starter.services'])

.directive('map', function(leeArchivoJSON, manejaMarcadores) {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {
      function initialize() {
        var datos_centro = "json/datos_centro.json";
        leeArchivoJSON.getData(datos_centro).then(
          function(data) {
            var datos = data;
            var mapOptions = {
              center: new google.maps.LatLng(datos[0].Latitud, datos[0].Longitud),
              zoom: 16,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var mapa = new google.maps.Map($element[0], mapOptions);
  
            $scope.onCreate({map: mapa});

            // Stop the side bar from dragging when mousedown/tapdown on the map
            google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
              e.preventDefault();
              return false;
            });

            var datos_marcadores = "json/datos_marcadores.json";
            manejaMarcadores.crea(mapa, datos_marcadores);
            var datos_ruta = "json/datos_ruta.json";
            //google.maps.TravelMode.DRIVING (Default) indicates standard driving directions using the road network.
            //google.maps.TravelMode.BICYCLING requests bicycling directions via bicycle paths & preferred streets.
            //google.maps.TravelMode.TRANSIT requests directions via public transit routes.
            //google.maps.TravelMode.WALKING
            var modo_viaje = google.maps.DirectionsTravelMode.WALKING;
            manejaMarcadores.ruta(mapa, datos_ruta, modo_viaje);

           });

        }

      if (document.readyState === "complete") {
        initialize($scope, $element, $attr);
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
});
